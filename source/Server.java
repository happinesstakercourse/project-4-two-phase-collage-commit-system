// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/* Server.java
 *
 * Center server node in two-phase commit group photo collage.
 * When a commit request is received by server (start transaction), the
 *   server would:
 *   1. save stable copy of collage image and source information
 *     NOTE: additional commit point, useful if recovery of transaction needed
 *           when server recover from crash. However, it is required that
 *           transaction get aborted if not previously commited.
 *   2. send out vote request to users according to source list
 *   3. make decision if one NO reply is received or all YES replies received
 *   4. commit the collage and transfer to notify stage
 *   5. send out notification to all users of the decision
 *   6. wait until all users acknowledge, end transaction
 *
 * Timeout and error recovery choice would be elaborated in design doc.
 */


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

public class Server implements ProjectLib.CommitServing,
        ProjectLib.MessageHandling {

    // 3000ms at most for one-way
    private static final int TIMEOUT = 6500;
    private static ProjectLib PL;

    // transaction ID, unique for each transaction
    private static int transID;

    /* information for Stage 1: ask vote */
    // reply status of each user
    private static HashMap<Integer, HashMap<String, Boolean>> transList =
            new HashMap<>();
    // source file list of each user
    private static HashMap<Integer, HashMap<String, String>> sourceFileList =
            new HashMap<>();

    /* information for Stage 2: notify */
    // users to notify for each transaction
    private static HashMap<Integer, Vector<String>> notifyList =
            new HashMap<>();
    // commit decision of each transaction
    private static HashMap<Integer, Boolean> notifyDecision =
            new HashMap<>();

    // locks for correct concurrent message handling
    private final static Object transListLock = new Object();
    private final static Object notifyListLock = new Object();

    // scheduler to patch ontimeout procedures
    private static ScheduledExecutorService executor;



    public static void main ( String args[] ) throws Exception {
        if (args.length != 1) throw new Exception("Need 1 arg: <port>");
        Server srv = new Server();

        // Recover state information from stable storage.
        // Make sure this happens before receiving message again, to avoid
        //   concurrency errors.
        transID = srv.recoverFromFailure() + 20;

        // Maintain maximum 3 working thread for timeout procedure, enough
        //   as performance not critical issue.
        executor = Executors.newScheduledThreadPool(3);

        PL = new ProjectLib( Integer.parseInt(args[0]), srv, srv);

        PL.fsync();

        // Resend notify messages to users in case some not sent before crash.
        srv.retransmitAfterRecovery();
    }

    @Override
    public void startCommit( String filename, byte[] img, String[] sources ) {

        // advance transaction ID, make sure lock is applied
        int curTransID;
        synchronized (transListLock) {
            curTransID = transID;
            transID++;
        }

        // execute main procedure to aggregate vote list and send vote requests
        askVote(sources, curTransID, img, filename);
	}

	@Override
	public boolean deliverMessage( ProjectLib.Message msg ) {

	    String user = msg.addr;
	    byte cmd = msg.body[0];

	    switch (cmd) {
            case 'V':
                // Vote Reply from user
                collectVote(user, new String(msg.body));
                break;
            case 'A':
                // Notify acknowledge from user
                collectAck(user, new String(msg.body));
                break;
            default:
                break;
        }

        return true;
	}

    /**
     * Main working procedure to:
     *   1. aggregate source files, the list of files for each user
     *   [2. save transaction information into stable storage]
     *
     * @param sources List of source files, format [a:1.jpg, b:3.jpg]
     * @param transID Current transaction ID
     * @param img Byte array of collage image data
     * @param filename Filename of the collage
     * @return Execution succeed or not
     */
	private boolean askVote(String[] sources, int transID,
                           byte[] img, String filename) {

        // Aggregate messages according to user node
        // msgList -> {'a':'1.jpg 2.jpg', 'c':'5.jpg'}
	    HashMap<String, String> msgList = new HashMap<>();

	    for (String source: sources) {
	        String[] parts = source.split(":");

	        if (msgList.containsKey(parts[0])) {
	            msgList.put(parts[0], msgList.get(parts[0]) + " " + parts[1]);
            } else {
	            msgList.put(parts[0], parts[1]);
            }
        }
        sourceFileList.put(transID, msgList);

	    // Initialize user reply status map to all false
        // Stage 1 transaction log format -> a:1.jpg 2.jpg;b:3.jpg;
        String transListLogStr = "";

        synchronized (transListLock) {
            if (!transList.containsKey(transID)) {
                transList.put(transID, new HashMap<>());
            }
            for (Map.Entry<String, String> entry : msgList.entrySet()) {
                transList.get(transID).put(entry.getKey(), false);
                transListLogStr += (entry.getKey()+":"+entry.getValue()+";");
            }
        }

        // Save Stage 1 log (optional)
        try {
            // save temp img to stable storage
            Path imgPath = Paths.get("TMPimg;" + transID + ";" + filename);
            Files.write(imgPath, img);

            // save temp tranaction list to stable storage
            Path logFile = Paths.get("TMPtrans;" + transID);
            Files.deleteIfExists(logFile);
            Files.write(logFile, transListLogStr.getBytes());

            // bring tmp file as master log file
            Files.move(logFile, Paths.get("trans;"+transID), ATOMIC_MOVE);
            Files.move(imgPath,
                    Paths.get("img;"+transID+";"+filename),
                    ATOMIC_MOVE);

        } catch (IOException eIO) {
            System.out.println("Server: IO Exception at Stage 1 save log...");
            return false;
        }

        PL.fsync();

        // Send out vote requests to users
        sendVoteRequest(img, transID);

        return true;
    }

    /**
     * Build vote request message and send to users.
     *
     * @param img Byte array of collage image data
     * @param transID Current transaction ID
     */
    private void sendVoteRequest(byte[] img, int transID) {

        // Vote request message format -> V;[transID]:[source file list];[img]
        for (Map.Entry<String, String> entry:
                sourceFileList.get(transID).entrySet()) {

            String msgHeader = "V;" + transID + ":" +
                    entry.getValue() + ";";
            byte[] msgBody = concatByteArray(msgHeader.getBytes(), img);

            ProjectLib.Message msg =
                    new ProjectLib.Message(entry.getKey(), msgBody);
            PL.sendMessage(msg);
        }

        // Register ontimeout procedure to abort transaction
        executor.schedule(new Runnable() {
            public void run() {
                cleanVoteRoundOnTimeout(transID);
            }
        }, TIMEOUT, TimeUnit.MILLISECONDS);
    }

    /**
     * Helper function to concatenate two byte arrays
     *
     * @param a First byte array
     * @param b Second byte array
     * @return Concatenated new byte array
     */
    private byte[] concatByteArray(byte[] a, byte[] b) {

        byte[] combined = new byte[a.length + b.length];
        System.arraycopy(a, 0, combined, 0, a.length);
        System.arraycopy(b, 0, combined, a.length, b.length);
        return combined;
    }

    /**
     * On timeout procedure to abort transaction.
     * We assume implicit NO reply on timeout in Stage 1, so that after timeout,
     *   if transaction not committed, it would be aborted.
     *
     * @param transID Transaction ID to abort
     */
    private void cleanVoteRoundOnTimeout(int transID) {
        synchronized (transListLock) {
            if (transList.containsKey(transID)) {
                // Due to concurrency issue, there would be situation that all
                //   YES replies are collected but collage not committed yet.
                // Check that some replies are not received before abort.
                for (Boolean decision: transList.get(transID).values()) {
                    if (!decision) {
                        commitStageTransfer(transID, false);
                        return;
                    }
                }
            }
        }
    }

    /**
     * Collect vote replies returned from users.
     * If decision can be made, commit the collage or abort, and transfer to
     *   Stage 2, and start notifying users the decision.
     *
     * @param user User ID source of reply
     * @param cmd Replied Message
     */
    private void collectVote(String user, String cmd) {

        // Vote reply msg format -> V;[vote];[transID]
	    String[] cmdParts = cmd.split(";");
	    String decision = cmdParts[1];
	    int transID = Integer.parseInt(cmdParts[2]);
	    boolean allAgree;

        synchronized (transListLock) {
            if (decision.equals("1")) {
                // change the reply status of corresponding user to true
                if (transList.containsKey(transID) &&
                        transList.get(transID).containsKey(user)) {
                    transList.get(transID).put(user, true);
                } else {
                    return;
                }

                // check if all users agreed to commit
                allAgree = true;
                for (boolean singleDecision : transList.get(transID).values()) {
                    if (!singleDecision) {
                        allAgree = false;
                        break;
                    }
                }

                // if all agreed, commit collage now
                if (allAgree) {
                    commitStageTransfer(transID, true);
                }
            } else {
                // one vote is "NO", abort immediately
                if (transList.containsKey(transID)) {
                    commitStageTransfer(transID, false);
                }
            }
        }
    }

    /**
     * Patch procedure to commit collage and transfer to Stage 2.
     *
     * @param transID Transaction ID to transfer
     * @param decision Commit or Abort
     */
    private void commitStageTransfer(int transID, boolean decision) {

        synchronized (notifyListLock) {
            // migrate state information for Stage 2
            transferStageNotify(transID);

            // commit point (Commit or Abort), save to stable storage
            commitCollage(decision, transID);
            PL.fsync();
        }

        // start notifying users
        notifyUser(decision, transID);
    }

    /**
     * Migrate state information for Stage 2.
     * Transfer information from reply status list (Stage 1) to notify list
     *   (Stage 2), because all users replied must be notified decision.
     * Remove Stage 1 information at the same time.
     *
     * @param transID Transaction ID to transfer
     */
    private void transferStageNotify(int transID) {

        notifyList.put(transID, new Vector<>());
        for (String src : transList.get(transID).keySet()) {
            notifyList.get(transID).add(src);
        }
        transList.remove(transID);
    }

    /**
     * Commit or Abort this transaction, and properly deal with image backup
     *   file in stable storage
     *
     * @param decision Commit decision
     * @param transID Transaction ID to hanlde
     */
    private void commitCollage(boolean decision, int transID) {

        // identify img and trans log file
        File curDir = new File(".");
        File[] listFiles = curDir.listFiles();
        File imgBackup = null;

        for (File file : listFiles) {
            if (file.getName().startsWith("img;" + transID)) {
                imgBackup = file;
                break;
            }
        }

        // img backup is lost, abort current transaction regardless of decision
        String imgFileName;
        if (imgBackup == null) {
            decision = false;
            imgFileName = "";
        } else {
            imgFileName = imgBackup.getName().split(";")[2];
        }

        // save notify decision
        notifyDecision.put(transID, decision);

        try {
            // prepare notify log file
            // format -> a;b;d
            // filename -> notify;[transID]
            String notifyStr = "";
            for (String notifyUser : notifyList.get(transID)) {
                notifyStr += (notifyUser + ";");
            }
            notifyStr += decision;

            // IMPORTANT: this order cannot be changed, otherwise if crash
            //   happens between log writing, recovery cannot be carried out
            //   properly.

            // 1. create notify list
            Files.write(Paths.get("TMPnotify;" + transID),
                    notifyStr.getBytes());
            Files.move(Paths.get("TMPnotify;" + transID),
                    Paths.get("notify;" + transID),
                    ATOMIC_MOVE);

            if (decision) {
                // 2. commit collage image if decide "YES"
                Files.move(imgBackup.toPath(),
                        Paths.get(imgFileName),
                        ATOMIC_MOVE);
                System.out.println("Transaction " + transID + " Commited.");
            } else{
                // 2. delete image backup if decide "NO"
                imgBackup.delete();
                System.out.println("Transaction " + transID + " Rejected.");
            }

            // 3. delete transaction list log use in Stage 1
            File transLog = new File("trans;" + transID);
            transLog.delete();

        } catch (IOException eIO) {
            System.out.println("IO Exception at server commit point");
        }
    }

    /**
     * Send notification message to users.
     *
     * @param decision Decision of this transaction
     * @param transID Transaction ID
     */
    private void notifyUser(boolean decision, int transID) {

        // notification message format -> [transID];[decision]
        String msg;
        if (decision) {
            msg = "D;1;" + transID;
        } else {
            msg = "D;0;" + transID;
        }

        synchronized (notifyListLock) {
            // all users have replied, transaction ended
            if (!notifyList.containsKey(transID)) {
                return;
            }

            for (String user : notifyList.get(transID)) {
                PL.sendMessage(new ProjectLib.Message(user, msg.getBytes()));
            }
        }

        // Register on timeout retransmission of notification messages
        executor.schedule(new Runnable() {
            @Override
            public void run() {
                notifyUser(decision, transID);
            }
        }, TIMEOUT, TimeUnit.MILLISECONDS);
    }

    /**
     * Collect acknowledge of users to notification message.
     *
     * @param user User ID source of acknowledge
     * @param cmd Acknowledge message
     */
    private void collectAck(String user, String cmd) {

        // Ack msg format -> A;[transID]
        int transID = Integer.parseInt(cmd.split(";")[1]);

        synchronized (notifyListLock) {
            if (notifyList.containsKey(transID)) {
                notifyList.get(transID).remove(user);

                if (notifyList.get(transID).size() == 0) {
                    // all user acknowledged
                    notifyList.remove(transID);
                    notifyDecision.remove(transID);

                    // delete Stage 2 log
                    File notifyLog = new File("notify;" + transID);
                    notifyLog.delete();
                }
            }
        }
    }

    /**
     * On server restart, try to recover state information from logs.
     *
     * @return Maximum past transaction ID
     */
    private int recoverFromFailure() {

        File curDir = new File(".");
        File[] listFiles = curDir.listFiles();

        int maxRecoverID = 0;
        // Save transactions recovered as Stage 2 transactions
        Vector<Integer> notifyRecovered = new Vector<>();

        // 1. first pass, delete all tmp files
        // All files starting with TMP are files half-written
        //   and broken, remove them
        for (File file: listFiles) {
            String filename = file.getName();
            if (filename.startsWith("TMP" + transID)) {
                file.delete();
            }
        }

        // 2. second pass, recover Stage 2 entries (notify list)
        for (File file: listFiles) {
            String filename = file.getName();

            if (filename.startsWith("notify;")) {
                String[] imgNameParts = filename.split(";");
                int transID = Integer.parseInt(imgNameParts[1]);

                if (transID > maxRecoverID) {
                    maxRecoverID = transID;
                }

                // recover notify list and decision
                recoverStage2Entry(transID);
                notifyRecovered.add(transID);
            }
        }

        // 3. third pass, abort Stage 1 entries (transaction list)
        for (File file: listFiles) {
            String filename = file.getName();

            if (filename.startsWith("img;")) {
                String[] imgNameParts = filename.split(";");
                int transID = Integer.parseInt(imgNameParts[1]);
                String collageName = imgNameParts[2];

                // transaction already recovered as second stage
                // broken Stage 1 log file, delete and ignore
                if (notifyRecovered.contains(transID)) {
                    file.delete();
                    continue;
                }

                if (transID > maxRecoverID) {
                    maxRecoverID = transID;
                }

                // if img backup file exist, then trans log MUST exist
                recoverStage1Entry(transID);

            } else if (filename.startsWith("trans;")) {
                int transID = Integer.parseInt(filename.split(";")[1]);

                if (notifyRecovered.contains(transID)) {
                    file.delete();
                }
            }
        }

        return maxRecoverID;
    }

    /**
     * Abort Stage 1 transaction listed by log.
     * Incomplete transaction still at Stage 1 (before commit point)
     *   would be aborted, transferred to Stage 2 and send NO decision
     *   to all users.
     *
     * @param transID Transaction to abort as Stage 1
     */
    private void recoverStage1Entry(int transID) {

        try {
            File tryFile = new File("trans;"+transID);
            if (!tryFile.exists()) {
                System.out.println("File manually deleted?");
                return;
            }

            // trans log format -> a:1.jpg 2.jpg;b:3.jpg;
            String transListStr = new String(Files.readAllBytes(
                    Paths.get("trans;" + transID)
            ));
            String[] logEntries = transListStr.split(";");

            Vector<String> notifyEntries = new Vector<>();
            for (String entry: logEntries) {
                String[] entryParts = entry.split(":");
                if (entryParts.length != 2) {
                    continue;
                }
                notifyEntries.add(entryParts[0]);
            }
            notifyList.put(transID, notifyEntries);

            // abort transaction and notify users
            commitCollage(false, transID);

        } catch (IOException eIO) {
            System.out.println("IO Exception at server recovery Stage 1");
        }
    }

    /**
     * Recover Stage 2 transaction listed by log.
     * Incomplete transaction already committed but waiting user acknowledge
     *   must be recovered and keep retransmission until all users reply.
     * This would cause non-termination if one user just lost forever, but
     *   this is the property of 2-phase commit.
     *
     * @param transID Transaction ID to recover as Stage 2
     */
    private void recoverStage2Entry(int transID) {

        try {
            String notifyListStr = new String(Files.readAllBytes(
                    Paths.get("notify;" + transID)
            ));

            // notify log format: a;b;d;[decision]
            String[] notifyEntries = notifyListStr.split(";");

            Vector<String> notifyEntry = new Vector<>();
            for (String user:notifyEntries) {
                // reaches log last entry -> decision of this transaction
                if (user.equals("false") || user.equals("true")) {
                    notifyDecision.put(transID, Boolean.parseBoolean(user));
                    break;
                } else {
                    notifyEntry.add(user);
                }
            }
            notifyList.put(transID, notifyEntry);

        } catch (IOException eIO) {
            System.out.println("IO Exception at server recovery Stage 2");
        }
    }

    /**
     * Resend all notification message after state information are fully
     *   recovered.
     */
    private void retransmitAfterRecovery() {

        for (int transID: notifyList.keySet()) {
            notifyUser(notifyDecision.get(transID), transID);
        }
    }
}

