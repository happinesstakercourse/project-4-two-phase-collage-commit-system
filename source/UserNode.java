// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/*
 * UserNode.java
 *
 * User node serves as receiver of two-phase commit procedure. It would not
 *   actively start new conversation. What it does is answering different
 *   message sent by server and reply with proper answer (vote request or
 *   decision notification).
 * Vote request:
 *   1. get vote request from server
 *   2. ask real user or read previous decision if possible
 *   3. send vote decision back to server
 * Notification:
 *   1. get decision of this transaction
 *   2. if YES, then delete corresponding source file so that it cannot be
 *        used again for another commit
 *   3. unlock pending information for those files
 *   4. respond with acknowledgement
 *
 * User node would not involve in timeout mechanism, as all abort or retransmit
 *   on timeout is handled by server.
 * User node recovers pending file locks on crash restart.
 */


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;

public class UserNode implements ProjectLib.MessageHandling {

    private final String myId;
	private static ProjectLib PL;

	private static HashMap<String, String> filePending = new HashMap<>();
	private final static Object filePendingLock = new Object();



	private UserNode( String id ) {
	    myId = id;
	}

    public static void main ( String args[] ) throws Exception {
        if (args.length != 2) throw new Exception("Need 2 args: <port> <id>");
        UserNode UN = new UserNode(args[1]);
        UN.recoverFromFailure();
        PL = new ProjectLib( Integer.parseInt(args[0]), args[1], UN );
    }

    @Override
	public boolean deliverMessage( ProjectLib.Message msg ) {

	    byte cmd = msg.body[0];

		switch (cmd) {

            case 'V':
                // Vote request from server
                decideVote(msg.body);
                break;
            case 'D':
                // Decision notification from server
                ackNotify(msg.body);
                break;
            default:
                break;
        }

        return true;
	}

    /**
     * Working procedure for certain user to decide whether to vote YES or NO
     *   to this transaction, based on collage image file and source list.
     *
     * @param cmd Vote request message from server
     */
	private void decideVote(byte[] cmd) {

	    // Find separator between message protocol info and img byte array
        int imgFence = -1;
	    for (int index = 2; index < cmd.length; index++) {
            if (cmd[index] == ';') {
                imgFence = index;
                break;
            }
        }

        // Separate and copy out information needed
        byte[] sourceInfoByte = new byte[imgFence - 2];
	    byte[] img = new byte[cmd.length - imgFence - 1];
	    System.arraycopy(cmd, 2, sourceInfoByte,
                0, sourceInfoByte.length);
	    System.arraycopy(cmd, imgFence + 1, img,
                0, img.length);

	    String sourceInfo = new String(sourceInfoByte);
	    String[] infoParts = sourceInfo.split(":");

	    String transID = infoParts[0];
	    String[] sources = infoParts[1].split(" ");


	    synchronized (filePendingLock) {
	        for (String source: sources) {
                // if file not exist, return NO vote immediately
	            File tryFile = new File(source);
	            if (!tryFile.exists()) {
	                sendVoteDecision(false, transID);
	                return;
                }

                // if decision made previously, reply with previous decision
	            if (filePending.containsKey(source)) {
	                if (filePending.get(source).equals(transID)) {
	                    // server retransmission of vote request,
                        // previouly replied yes, now reply yes again
                        // no need to commit again
                        sendVoteDecision(true, transID);
                    } else {
	                    // collision with blocking request
                        sendVoteDecision(false, transID);
                    }
	                return;
                }
            }

            // ask user whether to agree commit
            boolean decision = PL.askUser(img, sources);
            if (decision) {
                for (String source : sources) {
                    filePending.put(source, transID);
                }
                // only commit file pending status on yes,
                // as "NO" decision would not change pending state
                commitPendingStatus();
            }

            // notify server only after commit done,
            // then node failure after commit could recover pending state
            sendVoteDecision(decision, transID);
        }
    }

    /**
     * Send the reply of vote back to server.
     *
     * @param decision Decision made by user
     * @param transID Transaction ID of reply
     */
    private void sendVoteDecision(boolean decision, String transID) {
	    String msg;
	    if (decision) {
            msg = "V;1;" + transID;
        } else {
            msg = "V;0;" + transID;
            System.out.println("User at " + myId + " Reject " + transID);
        }

        PL.sendMessage(new ProjectLib.Message("Server", msg.getBytes()));
    }

    /**
     * Acknowledge decision notification sent by server, and delete source
     *   files used in this transaction if collage is committed.
     *
     * @param cmd Decision Notification Message
     */
    private void ackNotify(byte[] cmd) {
	    String cmdStr = new String(cmd);
	    String[] cmdStrParts = cmdStr.split(";");

	    String decision = cmdStrParts[1];
	    String transID = cmdStrParts[2];

        synchronized (filePendingLock) {
            // clear pending lock of source files regardless of decision
            Set<String> removeList = new HashSet<>();

            for (Map.Entry<String, String> entry : filePending.entrySet()) {
                if (entry.getValue().equals(transID)) {
                    removeList.add(entry.getKey());

                    // delete source files in stable storage if committed
                    if (decision.equals("1")) {
                        File localImg = new File(entry.getKey());
                        localImg.delete();
                    }
                }
            }
            filePending.keySet().removeAll(removeList);

            // refresh pending information log
            commitPendingStatus();
        }

        String ackMsg = "A;" + transID;
        PL.sendMessage(new ProjectLib.Message("Server", ackMsg.getBytes()));
    }

    /**
     * Save pending file information log in stable storage
     */
    private void commitPendingStatus() {

	    try {
	        // clear pending file if no file in pending status
	        if (filePending.isEmpty()) {
	            Files.deleteIfExists(Paths.get("filePending.log"));
            }
            // rewrite entire pending file log in temp copy
            // replace master log after entire write finished
            else {
                Path logFile = Paths.get("filePending-tmp.log");
                Files.deleteIfExists(logFile);
                String logString = "";
                for (Map.Entry<String, String> entry : filePending.entrySet()) {
                    logString +=
                            (entry.getKey() + "=" + entry.getValue() + ";");
                }
                Files.write(logFile, logString.getBytes());
                Files.move(logFile,
                        Paths.get("filePending.log"),
                        ATOMIC_MOVE);
            }
            PL.fsync();
        } catch (IOException eIO) {
	        System.out.println("User " + myId + ": IO Exception write log");
        }
    }

    /**
     * Recover file pending status from log in stable storage.
     */
    private void recoverFromFailure() {

	    try {
	        // delete uncompleted tmp log file
            Path logFile = Paths.get("filePending-tmp.log");
            Files.deleteIfExists(logFile);

            // recover from main backup log
            logFile = Paths.get("filePending.log");
            File logFileDesc = new File(logFile.toString());
            if (!logFileDesc.exists()) {
                return;
            }

            // pending log format -> 1.jpg=[transID];3.jpg=[transID];
            String filePendingString = new String(Files.readAllBytes(logFile));
            String[] entryStrs = filePendingString.
                    split(";");

            for (String entry:entryStrs) {
                String[] entryParts = entry.split("=");
                if (entryParts.length != 2) {
                    continue;
                }
                filePending.put(entryParts[0], entryParts[1]);
            }

        } catch (IOException eIO) {
	        System.out.println("User " + myId + ": IO Exception recover");
        }
    }

}

